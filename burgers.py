from proteus.iproteus import Profiling, TransportCoefficients
from proteus.iproteus import NumericalSolution, opts
Profiling.logLevel = 0
Profiling.verbose = True
import numpy as np

from proteus import default_p as p
# Physics
p.name = "burgers_1d"
p.nd = 1  # Two dimensions
p.L = [2*np.pi]
p.T = 1.0

# Will need Burgers + space dependant source term here
# What is v?
p.coefficients = TransportCoefficients.ViscousBurgersEqn(
    # v=[1.0],
    nu=1e-4,
    nd=1,
    nc=4)


def getDBC(x, flag):
    if x[0] == 1.0:
        return lambda x, t: 0.0
    else:
        return None

# Will need periodic here? #Two fixed zero values at the ends would do too!
p.dirichletConditions = dict((i, getDBC) for i in range(p.coefficients.nc))
p.advectiveFluxBoundaryConditions = {}


def getDFBC(x, flag):
    if x[0] == p.L[0]:
        return lambda x, t: 0.0

p.diffusiveFluxBoundaryConditions = {0: {0: getDFBC}}
p.periodicDirichletConditions = None


# Will need a perturbation here
class IC:
    def __init__(self):
        pass

    def uOfXT(self, x, t):
        return np.sin(x[0])

p.initialConditions = dict((i, IC()) for i in range(p.coefficients.nc))

# Numerics
from proteus import default_n as n
import proteus as pr
n.timeIntegration = pr.TimeIntegration.BackwardEuler_cfl
n.stepController = pr.StepControl.Min_dt_cfl_controller
n.runCFL = 0.99
n.femSpaces = dict(
    (i, pr.FemTools.DG_AffineLinearOnSimplexWithNodalBasis)
    for i in range(p.coefficients.nc))
n.elementQuadrature = pr.Quadrature.SimplexGaussQuadrature(p.nd, 4)
n.elementBoundaryQuadrature = pr.Quadrature.SimplexGaussQuadrature(p.nd-1, 4)
n.subgridError = None
n.shockCapturing = None
n.numericalFluxType =\
    pr.NumericalFlux.ConvexOneSonicPointNumericalFlux
n.conservativeFlux = {0: 'dg'}
n.nn = 100
n.tnList = [float(i)/200 for i in range(41)]
n.parallelPartitioningType = pr.MeshTools.MeshParallelPartitioningTypes.element
n.nLayersOfOverlapForParallel = 1
# n.periodicDirichletConditions = None

n.multilevelNonlinearSolver = pr.NonlinearSolvers.Newton
n.maxLineSearches = 0
# Convergence criteria
n.maxNonlinearIts = 50
n.tolFac = 0.0
n.nl_atol_res = 1.0e-4
# Linear solver selection
n.matrix = pr.LinearAlgebraTools.SparseMatrix
n.multilevelLinearSolver = pr.LinearSolvers.KSP_petsc4py
# PETSc solver configuration
from petsc4py import PETSc
OptDB = PETSc.Options()
OptDB.setValue("ksp_type", "gmres")
OptDB.setValue("pc_type", "jacobi")
# convergence criteria
n.linearSolverConvergenceTest = 'r-true'
n.linTolFac = 0.001
n.l_atol_res = 0.001*n.nl_atol_res
# Operator splitting
from proteus import default_s, default_so
so = default_so
so.name = p.name
so.sList = [default_s]
so.tnList = n.tnList
# Initializing Numerical Solution Object
ns = NumericalSolution.NS_base(so, [p], [n], so.sList, opts)

failed = ns.calculateSolution('burgers_run1')
assert(not failed)
